public class MyCharacter {
    private char nilai; //variabel instance nilai yang akan menyimpan nilai karakter

    //Zahra Aura Nabila - 235150701111004 - TI A

    public MyCharacter(char nilai) {
        this.nilai = nilai;
    } //Konstruktor menerima karakter sebagai argumen dan menginisialisasi variabel
    // instance nilai dengan nilai yang diberikan

    public char nilaiChar() {
        return nilai;
    } //mengembalikan karakter yang disimpan dalam objek MyCharacter

    public int bandingkan(MyCharacter karakterLain) {
        return this.nilai - karakterLain.nilai;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MyCharacter)) {
            return false;
        }
        MyCharacter karakterLain = (MyCharacter) obj;
        return this.nilai == karakterLain.nilai;
    } //memeriksa apakah objek MyCharacter saat ini sama dengan objek lain yang diberikan.
    // true jika kedua objek memiliki nilai karakter yang sama, dan false jika tidak.

    public static boolean apakahDigit(char ch) {
        return ch >= '0' && ch <= '9';
    }

    public static boolean apakahHuruf(char ch) {
        return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
    }

    public static boolean apakahHurufAtauDigit(char ch) {
        return apakahHuruf(ch) || apakahDigit(ch);
    }

    public static boolean apakahHurufKecil(char ch) {
        return ch >= 'a' && ch <= 'z';
    }

    public static boolean apakahHurufBesar(char ch) {
        return ch >= 'A' && ch <= 'Z';
    }

    public static char keHurufKecil(char ch) {
        if (apakahHurufBesar(ch)) {
            return (char) (ch + 32);
        }
        return ch;
    }

    public static char keHurufBesar(char ch) {
        if (apakahHurufKecil(ch)) {
            return (char) (ch - 32);
        }
        return ch;
    }

    public static MyCharacter valueOf(char ch) {
        return new MyCharacter(ch);
    } //mengembalikan objek MyCharacter yang dibuat dari karakter yang diberikan.

    public String toString() {
        return String.valueOf(nilai);
    } //mengembalikan representasi string dari karakter yang disimpan dalam objek MyCharacter.

    public static void main(String[] args) {
        MyCharacter karakterSaya = new MyCharacter('A');
        System.out.println("Karakter: " + karakterSaya.nilaiChar());
        System.out.println("Apakah digit: " + MyCharacter.apakahDigit('5'));
        System.out.println("Apakah huruf: " + MyCharacter.apakahHuruf('x'));
        System.out.println("Apakah huruf kecil: " + MyCharacter.apakahHurufKecil('k'));
        System.out.println("Apakah huruf besar: " + MyCharacter.apakahHurufBesar('F'));
        System.out.println("Ke huruf kecil: " + MyCharacter.keHurufKecil('H'));
        System.out.println("Ke huruf besar: " + MyCharacter.keHurufBesar('d'));
        System.out.println("Apakah huruf atau digit: " + MyCharacter.apakahHurufAtauDigit('9'));
    } //menguji fungsionalitas kelas MyCharacter, menciptakan objek MyCharacter dengan karakter 'A',
    // kemudian mencetak hasil dari berbagai metode yang tersedia untuk karakter tersebut.
}
